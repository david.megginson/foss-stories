FOSS will hunt you down
=======================

If you create a FOSS project, it never dies. You might lose interest and try to move on, but years or decades later, people will still be reaching out to you to report a critical bug in an old library that they're using in critical infrastructure.

A successful FOSS project is a lifelong commitment.
