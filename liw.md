# Lars Wirzenius

I maintain a few minor personal projects that get fairly little
attention. I get an issue report or other contact about once a month
in total. The obscurity means there are few demands on my time, but
also, of course, fairly little positive feedback or outside
contributions.

I used to have some personal projects that were more popular. The
level of interaction and effort required was manageable, for me, but
I'm happier this way.
